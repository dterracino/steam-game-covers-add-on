# Steam Game Covers (Add-on)

Burn your Steam game backups to a CD that&#39;s as great as the game itself. For that you need really good covers. This add-on adds a &#34;Cover&#34;-tab to the Steam games page. 
It include: 
- Disc faces 
- Jewel cases 
- Amary cases 
- BlueRay cases